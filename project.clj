(defproject policy-search "0.1.0-SNAPSHOT"
  :description "Black Box Policy Search with Probabilistic Programming"
  :url "https://bitbucket.org/probprog/black-box-policy-search"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/data.csv "0.1.3"]
                 [org.clojure/data.json "0.2.6"]
                 [net.mikera/core.matrix "0.49.0"]
                 [net.mikera/core.matrix.stats "0.7.0"]
                 [net.mikera/vectorz-clj "0.43.1"]
                 [anglican "0.9.0"]]
  :jvm-opts ["-Xmx2048m"]
  :main policy-search.core
  :profiles {:uberjar {:aot [policy-search.core guess-who.core rockwalk.core]}})

