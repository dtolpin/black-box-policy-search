(ns guess-who.heuristics
  "non-sampling based heuristics for the expected value of an action"
  (:use guess-who.model
        [guess-who.data :only [+entities+ +questions+]]))

(defn myopic-voi
  "calculates a myopic estimate of the value of information"
  [info question]
  (let [belief (posterior-belief info)
        theta (response-probability belief question)
        u-true (relative-utility belief
                                 (posterior-belief
                                  (update-info info
                                               question
                                               true)))
        u-false (relative-utility belief
                                  (posterior-belief
                                   (update-info info
                                                question
                                                false)))]
    (+ (* theta u-true)
       (* (- 1 theta) u-false))))

(defn recursive-voi
  "calculates a recursive estimate of the value of information"
  [info depth question]
  (let [belief (posterior-belief info)
        candidates (max-entries belief)
        theta (response-probability belief question)
        vois (map (fn [response]
                    (let [belief (posterior-belief info)
                          new-info (update-info info question response)
                          new-belief (posterior-belief new-info)]
                      (+ (relative-utility belief new-belief)
                         (if (<= depth 1)
                           0.
                           (reduce max
                                   (map (fn [next-question]
                                          (recursive-voi new-info
                                                         (dec depth)
                                                         next-question))
                                        +questions+))))))
                  [true false])]
    (reduce +
            (map * [theta (- 1.0 theta)] vois))))


