(ns guess-who.core
  (:gen-class)
  (:require [clojure.core.matrix :as mat]
            [clojure.tools.cli :as cli]
            [clojure.data.json :as json]
            [guess-who
             [policy :refer [make-policy]]
             [trial :refer [trial test-sweep]]])
  (:use [anglican runtime emit]))

;; query parameters
(def +policy-types+ [:discounted-belief])
(def +number-of-questions+ 4)
(def +inverse-temperature+ 10.0)

;; policy learning parameters
(def +number-of-particles+ 1000)
(def +numsteps-range+ [1 2 5 10 20 50 100 200 500 1000])
(def +base-stepsize+ 0.1)
(def +adagrad+ 0.9)
(def +robbins-monro+ 0.5)

(def +number-of-test-sweeps+ 4)

(def +baseline-types+ [:uniform :myopic-voi])

(extend-protocol
  json/JSONWriter
  mikera.matrixx.Matrix
  (-write
   [object out]
   (json/-write (mat/to-nested-vectors object) out))
  mikera.vectorz.Vector
  (-write
   [object out]
   (json/-write (mat/to-nested-vectors object) out))
  anglican.runtime.gamma-distribution
  (-write
   [dist out]
   (json/-write (select-keys dist [:shape :rate]) out))
  anglican.runtime.beta-distribution
  (-write
   [dist out]
   (json/-write (select-keys dist [:a :b]) out)))

(defn run-trials
  [& {policy-types :policy-types
      number-of-questions :number-of-questions
      numsteps-range :numsteps-range
      number-of-test-sweeps :number-of-test-sweeps
      number-of-particles :number-of-particles
      base-stepsize :base-stepsize
      adagrad :adagrad
      robbins-monro :robbins-monro
      :or {policy-types +policy-types+
           number-of-questions +number-of-questions+
           numsteps-range +numsteps-range+
           number-of-test-sweeps +number-of-test-sweeps+
           number-of-particles +number-of-particles+
           base-stepsize +base-stepsize+
           adagrad +adagrad+
           robbins-monro +robbins-monro+}}]
  (doseq [policy-type policy-types
          number-of-steps numsteps-range]
    (let [[rewards policies proposals] (trial policy-type
                                              number-of-questions
                                              number-of-particles
                                              number-of-steps
                                              number-of-test-sweeps
                                              :base-stepsize base-stepsize
                                              :adagrad adagrad
                                              :robbins-monro robbins-monro)
          file-name (format "results/guess-who/guess-who-%s-depth%d-particles%d-rho%.1f-gamma%.1f-kappa%.1f-steps%05d.json"
                            (name policy-type)
                            number-of-questions
                            number-of-particles
                            base-stepsize
                            adagrad
                            robbins-monro
                            number-of-steps)]
      (println (format "writing: %s" file-name))
      (spit file-name
            (json/write-str {:rewards rewards
                             :policy-type (name policy-type)
                             :policy-parameters (map #(into {} %) policies)
                             :proposal-parameters (zipmap (keys proposals)
                                                          (map second
                                                               (vals proposals)))
                             :depth number-of-questions
                             :training-parameters {:number-of-particles number-of-particles
                                                   :number-of-steps number-of-steps
                                                   :base-stepsize base-stepsize
                                                   :adagrad adagrad
                                                   :robbins-monro robbins-monro}
                             :number-of-test-sweeps number-of-test-sweeps})))))

(defn run-baselines
  [& {policy-types :policy-types
      number-of-questions :number-of-questions
      number-of-test-sweeps :number-of-test-sweeps
      number-of-replicates :number-of-replicates
      :or {policy-types +baseline-types+
           number-of-questions +number-of-questions+
           number-of-test-sweeps +number-of-test-sweeps+
           number-of-replicates +number-of-particles+}}]
  (doseq [policy-type policy-types]
    (let [rewards (repeatedly number-of-replicates
                              #(test-sweep (make-policy policy-type nil)
                                           number-of-questions
                                           number-of-test-sweeps))
          file-name (format "results/guess-who/guess-who-%s-depth%d.json"
                            (name policy-type)
                            number-of-questions)]
      (spit file-name
            (json/write-str {:rewards rewards
                             :policy-type (name policy-type)
                             :depth number-of-questions
                             :number-of-test-sweeps number-of-test-sweeps})))))

(def cli-options
  [["-q" "--number-of-questions NUMBER" "Number of questions"
    :parse-fn #(Integer/parseInt %)
    :default +number-of-questions+]

   ["-t" "--number-of-test-sweeps NUMBER" "Number of test sweeps"
    :parse-fn #(Integer/parseInt %)
    :default +number-of-test-sweeps+]

   ["-r" "--numsteps-range RANGE" "Range of numbers of steps"
    :parse-fn #(read-string (str "[" % "]"))
    :default +numsteps-range+]

   ["-a" "--adagrad GAMMA" "Decay rate for AdaGrad/RMSProp"
    :parse-fn #(Double/parseDouble %)
    :default +adagrad+]

   ["-k" "--robbins-monro KAPPA" "Decay rate for step size"
    :parse-fn #(Double/parseDouble %)
    :default +robbins-monro+]

   ["-h" "--help" "Print usage summary and exit"]])

(defn -main
  "runs trials or baselines from the command line"
  [& args]
  (let [{:keys [options arguments errors summary] :as parsed-options}
        (cli/parse-opts args cli-options)
        summary (format "baselines|trials OPTIONS\n%s" summary)]
    (cond
      (:help options) (println summary)
      errors (println errors)
      (empty? arguments) (println summary)

      :else
      (apply (var-get (find-var (symbol "guess-who.core"
                                        (format "run-%s" (first arguments)))))
             (apply concat options)))))
