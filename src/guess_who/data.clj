(ns guess-who.data
  (:require [clojure.java.io :as io]
            [clojure.data.csv :as csv]))

(defn parse-field
  "parses a string into a value. symbols and booleans are returned as keywords"
  [string]
  (let [value (read-string string)]
    (if (symbol? value)
      (keyword value)
      value)))

(defn select-values
  "returns a vector of values for keys ks from the hash-map m"
  [m ks]
  (reduce (fn [vs k]
            (if-let [v (m k)]
              (conj vs v)
              vs))
          []
          ks))

(defn read-entities
  "reads a map of entities {id {attr value}} from a url to a csv file"
  [url & indices]
  (with-open [reader (io/reader url)]
    (let [[headers & rows] (mapv #(mapv parse-field %)
                                 (csv/read-csv reader))
          records (map (fn [row]
                         (into {} (map vector headers row)))
                       rows)
          entities (into {}
                         (map (fn [record]
                                (let [id (if (> (count indices) 1)
                                           (select-values record indices)
                                           (record (first indices)))
                                      attrs (reduce dissoc record indices)]
                                  [id attrs]))
                              records))]
      entities)))

(defn matches
  "returns subset of entities that match question response"
  [entities question response]
  (let [[attr value] question]
    (into {}
          (filter (fn [[id entity]]
                    (= (= (entity attr) value) response))
                  entities))))

(defn attribute-values
  "returns a map {attr [value]} of unique attribute values"
  [entities]
  (reduce
    (fn [avs [id attrs]]
      (reduce
        (fn [avs [attr value]]
          (assoc avs
            attr (conj (avs attr #{}) value)))
        avs
        attrs))
    {}
    entities))

(defn minimal-questions
  "returns the minimal set of questions (excluding redundant questions)
  to fully distinguish a set of entities"
  [entities]
  (let [attr-vals (attribute-values entities)]
    (reduce
      (fn [qs [a vs]]
        (cond
          ;; ignore single-valued attributes
          (= (count vs) 1)
          qs
          ;; add a single question for binary attributes
          (= (count vs) 2)
          (conj qs [a (first (sort vs))])
          ;; inlcude one question per value for other attributes
          :else
          (reduce conj
                  qs
                  (map vector
                       (repeat (count vs) a)
                       (sort vs)))))
      []
      (sort attr-vals))))

(def +entities+
  (into (sorted-map)
        (read-entities (io/resource "guess-who/entities.csv") :id)))

(def +questions+
  (minimal-questions +entities+))

