(ns rockwalk.model
  "RockSample POMDP solving using Probabilistic Inference"
  (:require [clojure.tools.cli :as cli])
  (:use [anglican runtime emit
         [state :only [get-predicts]]
         [core :only [doquery]]]
        [rockwalk data]))

(defdist factor [] []
  (sample [this] nil)
  (observe [this value] value))

(def +factor+ (factor))

;; We implement an offline algorithm for the RockSample POMDP.
;; At every step, the agent selects the closest stone,
;; senses it, and either goes for the stone or discards it.
;; When there are no stones left, the agent heads straight
;; to the right edge.

;; A problem instance is defined by
;;  - the field size (n×n);
;;  - locations and values of rocks (good/bad).
;;  - location of the agent.

(defrecord state [n rocks x y])
;; where `rocks' is a hash map
;;   [^Integer x ^Integer y] -> ^Boolean good

;; The agent always starts at the middle of the left edge.
;; The goal state is beyond the right edge.

(defn goal?
  "true if the state is a goal state"
  [state]
  (= (:x state) (:n state)))

;; The sensor returns a noisy observation of rock value. The
;; accuracy decreases exponentially with the distance. At zero
;; distance, the sensor always returns the correct value. At the
;; half-efficiency distance (hed), the correct value is returned
;; with probability 0.75. At infinity, the correct and the
;; incorrect values are equally probable.

(defn accuracy
  "computes the probability of returning the correct rock value
  by the sensor"
  [state x y hed]
  (let [d (let [dx (- (:x state) x)
                dy (- (:y state) y)]
            (Math/sqrt (+ (* dx dx) (* dy dy))))
        efficiency (Math/pow 0.5 (/ d hed))]
    (* 0.5 (+ efficiency 1.))))

;; The robots moves rectilinearly, the manhattan distance is
;; chosen to choose a rock and compute the reward.

(defn distance
  "distance between the current and the next location"
  [state x y]
  (+ (Math/abs (- (:x state) x))
     (Math/abs (- (:y state) y))))

;; In the original formulation of RockSample the moves are free,
;; so the optimum policy is to go to every rock, know its value
;; with certainty, and sample if good. This problem formulation
;; still works for assessing value iteration algorithms because
;; of implicitly assumed reward discounting. However, a sound
;; problem formulation would either state the discounting factor
;; explicitly, limit the number of moves, or incur a cost on
;; every move.
;;
;; Here, we modify the problem formulation, so that in addition
;; to the rewards for sampling a good rock and for reaching the
;; right edge, there is a penalty for every move. All moves in
;; our space of policies are legal, and the robot never samples
;; a bad rock.

(def +sample-reward+    10.)
(def +move-reward+      -1.)
(def +goal-reward+      10.)

(defn goto
  "goes to a target location;
  samples the rock in that target location
  if the rock is there and good;
  returns the updated state and the reward"
  [state [x y :as loc]]
  [(assoc state
     :x x :y y
     :rocks (dissoc (:rocks state) loc))
   (+ (* (distance state x y) +move-reward+)
      (if ((:rocks state) loc)
        +sample-reward+
        0.0))])

;; A rock can be removed without going to the new location,
;; just because it is not deemed worth the attention.

(defn discard
  "discards the rock, returns the updated state"
  [state loc]
  (assoc state :rocks (dissoc (:rocks state) loc)))

;; At every step, the next rock is the closest rock
;; in the left-most column containing rocks

(defn next-rock
  "Returns the coordinates of the next rock"
  [state]
  (loop [nloc nil
         locs (keys (:rocks state))]
    (if (seq locs)
      (let [[[x y :as loc] & locs] locs
            [nx ny] nloc]
        (if (or (nil? nloc)
                (< x nx)
                (and (= x nx)
                     (< (distance state x y)
                        (distance state nx ny))))
          (recur loc locs)
          (recur nloc locs)))
      nloc)))

;; Now we can define the query that learns the thesholds.
(with-primitive-procedures [goal? accuracy distance
                            next-rock goto discard]
  (defquery rockwalk
    "rockwalk policy learning"
    [instance hed scale]
    (let [rocks (into
                 {}
                 (map (fn [[loc _]]
                        [loc (sample
                              [:rock loc]
                              (flip 0.5))])
                      (:rocks instance)))]
      (loop [state (assoc instance
                     :rocks rocks)
             visited []
             reward 0]
        (if (goal? state)
          (do
            ;; (observe +factor+ (* reward scale))
            (predict :visited visited)
            (predict :reward reward))
          (let [loc (next-rock state)]
            (if (nil? loc)
              ;; no rocks left, go to the goal
              (let [goal [(:n state) (:y state)]
                    [state r] (goto state goal)]
                (observe +factor+ (* r scale))
                (recur state
                       visited
                       (+ reward r)))
              (let [;; sample sensor reading for next rock
                    good (sample
                          [:sense [(:x state) (:y state)]]
                          (flip
                           (let [[x y] loc]
                             (if (get (:rocks state) loc)
                               (accuracy state x y hed)
                               (- 1. (accuracy state x y hed))))))
                    ;; decide whether to visit the rock
                    ;; (this is the policy choice)
                    visit (sample
                           [:policy [(:x state) (:y state)] loc good]
                           (tag
                            :policy
                            (flip 0.5)))]
                (if visit
                  ;; goto to rock, gain reward if rock is good
                  (let [[state r] (goto state loc)]
                    (observe +factor+ (* r scale))
                    (recur state
                           (conj visited loc)
                           (+ reward r)))
                  ;; remove rock from list of visitable rocks
                  (let [state (discard state loc)]
                        (recur state
                               visited
                               reward)))))))))))
